var app = require('express');
var server = require('http').Server(app);
var io = require('socket.io')(server);
var say = require('say');

server.listen(4040, function () {
    console.log("Server is now running!");
});

io.on('connection', function (socket) {
    console.log("Client connected!");

    socket.on('message', function (data) {
        for (var key in data) {
            say.speak(data[key], 'Alex', 1.0, function (err) {
                if (err)
                    return console.error(err);

                socket.emit('success', JSON.parse("{\"id\":" + key + "}"));
            });
        }
    });

    socket.on('disconnect', function () {
        console.log("Client disconnected!");
    });
});